# coding: utf-8

import numpy as np

import torch
import torch.utils.data

import torchvision
import torchvision.models
import torchvision.transforms
import albumentations
from glob import glob
import imageio
import numpy
from torch.utils.data.dataset import Dataset
import torchvision.transforms.functional as TF
import copy
import cv2

def get_train_val(agl_dirs, no_agl_dirs, train=0.8):
    agl_files = []
    no_agl_files = []

    for agl_dir in agl_dirs:
        files = glob(agl_dir + "/*") 
        agl_files.extend(files)

    for no_agl_dir in no_agl_dirs:
        files = glob(no_agl_dir + "/*") 
        no_agl_files.extend(files)
    x = agl_files + no_agl_files
    '''
    maxval_axis, argmax_axis = max(x.shape[1:]), np.argmax(x.shape[1:]) + 1
    padding = [[0, 0], [0, 0], [0, 0], [0, 0]] 
    padding[3 - argmax_axis] = [abs(maxval_axis - x.shape[3 - argmax_axis])//2,
                                abs(maxval_axis - x.shape[3 - argmax_axis])//2]
    padding = tuple([tuple(x) for x in padding])
    print(padding)
    x = np.pad(x, padding, 'constant', constant_values=0)
    '''
    agl_label = np.ones(len(agl_files))
    no_agl_label = np.zeros(len(no_agl_files))
    y = np.append(agl_label, no_agl_label)

    permutation = np.random.permutation(np.array(list(range(len(y)))))
    x = [x[i] for i in permutation]
    y = y[permutation]
    size = len(y)
    x_train, y_train = x[:int(train * size)], y[:int(train * size)]
    x_val, y_val = x[int(train * size):], y[int(train * size):]

    return x_train, y_train, x_val, y_val

def getint(name):
    return int(''.join(c for c in name if c.isdigit()))

class AglLoader(Dataset):
    def __init__(self, x, y, val=False):
        self.x = copy.deepcopy(x)
        self.y = copy.deepcopy(y)
        self.data_len = len(y)
        
        if (val == False):
            self.transforms = [
                albumentations.RandomBrightness(limit=(-0.4, 0.4)),
                albumentations.RandomContrast(limit=(-0.4, 0.4)),
                albumentations.RandomGamma()
            ]
        else:
            self.transforms = [
            ]

    def __getitem__(self, index):
        image, label = imageio.imread(self.x[index]), self.y[index]
        #mean = np.array([0.4914, 0.4822, 0.4465])
        #std = np.array([0.2470, 0.2435, 0.2616])
        
        if (len(self.transforms)):
            for transform in self.transforms:
                result = transform(image=image)
                image = result['image']

        maxval_axis, argmax_axis = max(image.shape), np.argmax(image.shape)
        padding = [[0, 0], [0, 0], [0, 0]] 
        padding[1 - argmax_axis] = [abs(maxval_axis - image.shape[1 - argmax_axis])//2,
                                    abs(maxval_axis - image.shape[1 - argmax_axis])//2]
        padding = tuple([tuple(x) for x in padding])
        image = np.pad(image, padding, 'constant', constant_values=0)
        image = cv2.resize(image, (112, 112))
        gray = image #np.expand_dims(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY), -1)
        return TF.to_tensor(gray).float(), int(label)

    def __len__(self):
        return self.data_len
