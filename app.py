#!/usr/bin/env python
from importlib import import_module
import os
from flask import Flask, render_template, Response
from flask_cors import CORS, cross_origin
import sys
import json
import datetime
import random

# import camera driver
# if os.environ.get('CAMERA'):
#     Camera = import_module('camera_' + os.environ['CAMERA']).Camera
# else:
#     from camera import Camera

# Raspberry Pi camera module (requires picamera package)
# from camera_pi import Camera

# from camera_opencv import Camera
from camera_analizer import Camera
import os

# video_path = 'ch01_20190520083721.mp4'
video_path = 'videos/ch01_15_20.mp4'
# video_path = 'videos/ch05_60_65.mp4'
# video_path = 'videos/ch11_30_35.mp4'
os.environ['OPENCV_CAMERA_SOURCE'] = video_path


label_path = 'videos/label_ch01.txt'
# label_path = 'videos/label_ch05.txt'
# label_path = 'videos/label_ch11.txt'
os.environ['OPENCV_LABEL_SOURCE'] = label_path

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# frames = []

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()

        # if random.random() > 0.999:
        #     frames.append(frame)

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed/0')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/detected_frames')
@cross_origin()
def get_detected_frames():
    res = []
    for i, item in enumerate(Camera.detected_frames):
        res.append({
            'src': '/frame/' + str(i),
            'time': item['time']
        })

    return json.dumps(res)

@app.route('/frame/<frame_id>')
@cross_origin()
def get_frame(frame_id):
    frame_id = int(frame_id)

    frames = Camera.detected_frames
    frame_response = frames[frame_id]['data']

    return Response(frame_response, mimetype='image/jpeg')


if __name__ == '__main__':
    # print (os.environ.get('OPENCV_CAMERA_SOURCE'))
    labels = ['videos/label_ch01.txt', 'videos/label_ch05.txt', 'videos/label_ch11.txt']
    videos = ['videos/ch01_15_20.mp4', 'videos/ch05_60_65.mp4', 'videos/ch11_30_35.mp4']

    idx = int(sys.argv[1])
    port = int(sys.argv[2])

    os.environ['OPENCV_CAMERA_SOURCE'] = videos[idx]
    os.environ['OPENCV_LABEL_SOURCE'] = labels[idx]

    app.run(host='0.0.0.0', port=port, threaded=True)