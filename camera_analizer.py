import os
import cv2 as cv
from base_camera import BaseCamera
import video
import numpy as np
from skimage import util
from joblib import dump, load
from skimage.feature import blob_dog
import datetime

def read_points(filename):
    with open(filename) as ifile:
        out = []
        for line in ifile.readlines():
            a, b = line.split(' ')[0:2]
            a = int(a)
            b = int(b)
            out.append((a, b))

    return out

out_res = (112, 112)

def projectMap(img, M):
    warp = cv.warpPerspective(img, M, out_res)
    return warp

def get_feature(image):
    c = blob_dog(image, threshold=.2)
    return [np.sum(c[:,2]), len(c)]

def drawpoly(img, p, color):
    if color == 'R':
        clr = (0,255,0)
        # clr = (0,0,255)
    else:
        clr = (0,0,255)
        # clr = (0,255,0)

    return cv.polylines(img, np.array([p], dtype=np.int32),True, clr, thickness=5)

def add_label(labels, label, n):
    if (len(labels) == n):
        del labels[0]
    labels.append(label)

class Analizer(object):
    def __init__(self, lfile):
        print ("init")
        self.src = read_points(lfile)
        fsrc = np.array(self.src, dtype=np.float32)
        dst = np.array(((0, 0), (out_res[0] - 1, 0), out_res, (0, out_res[1] - 1)), dtype=np.float32)
        self.M = cv.getPerspectiveTransform(fsrc, dst)
        self.clf = load('model.bin')

    def analyze(self, img):
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        gray = projectMap(gray, self.M)
        gray = util.invert(gray)

        label = self.clf.predict([get_feature(gray)])

        return label

    def get_src_points(self):
        return self.src

class Camera(BaseCamera):
    video_source = 0
    analyzer = None
    detected_frames = []

    def __init__(self):
        if os.environ.get('OPENCV_CAMERA_SOURCE'):
            Camera.set_video_source(os.environ['OPENCV_CAMERA_SOURCE'])
            print (os.environ['OPENCV_CAMERA_SOURCE'])
        super(Camera, self).__init__()

    @staticmethod
    def set_video_source(source):
        Camera.video_source = source
        label_src = os.environ['OPENCV_LABEL_SOURCE']
        Camera.analyzer = Analizer(label_src)

    @staticmethod
    def frames():
        camera = video.create_capture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        labels = []
        buffer = []
        while True:
            # read current frame
            _, img = camera.read()

            label = Camera.analyzer.analyze(img)
            add_label(labels, label, 20)

            res = (sum(labels) > 13)

            if res:
                clr = 'G'
            else:
                clr = 'R'

            src = Camera.analyzer.get_src_points()
            img = drawpoly(img, src, clr)

            jpg = cv.imencode('.jpg', img)[1].tobytes()
            
            # print (res)
            if res:
                buffer.append(jpg)
                # Camera.detected_frames.append(jpg)
            else:
                if len(buffer) != 0:
                    Camera.detected_frames.append({
                        'data':buffer[0],
                        'time': str(datetime.datetime.now())
                    })
                buffer = []

            # encode as a jpeg image and return it
            yield jpg

if __name__ == "__main__":
    video_path = 'ch01_20190520083721.mp4'
    analyzer = Analizer('label2.txt')
    camera = video.create_capture(video_path)
    while True:
        # read current frame
        _, img = camera.read()

        res = analyzer.analyze(img)

        print (res)
        # src = Camera.analyzer.get_src_points()
        # img = drawpoly(img, src)

        # encode as a jpeg image and return it
        # yield cv.imencode('.jpg', img)[1].tobytes()
        cv.imshow('Image', img)
        cv.waitKey(1)