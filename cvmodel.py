#%%
# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import json
from matplotlib import pyplot as plt
from skimage.io import imread
from skimage import color
from skimage.transform import resize
from skimage.feature import hog, canny, blob_log, blob_dog
from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report,accuracy_score
from skimage.morphology import closing
from skimage import util 
from joblib import dump, load


# Input data files are available in the "../
# input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

from subprocess import check_output

pos_list = check_output(["ls", "./pos"]).decode("utf8")
neg_list = check_output(["ls", "./neg"]).decode("utf8")

# Any results you write to the current directory are saved as output.

# %%
pos_list = pos_list.splitlines()
neg_list = neg_list.splitlines()
pos_list = ['pos/' + x for x in pos_list]
neg_list = ['neg/' + x for x in neg_list]

#%%

pos_img = np.array([imread(x) for x in pos_list])
neg_img = np.array([imread(x) for x in neg_list])

imgs = np.concatenate((pos_img, neg_img))
print(imgs.shape)

labels = np.concatenate((np.ones((len(pos_img))), np.zeros((len(neg_img)))))
#%%
def preprocess(img):
    #img = resize(img, (64,64))
    kernel = np.ones((5,5))
    img = color.rgb2gray(img)
    #img = closing(img, kernel)
    return util.invert(img)

data = imgs
data_gray = [ preprocess(i) for i in data]
plt.imshow(data_gray[51])
#%%
def get_feature(image):
    c = blob_dog(image, threshold=.2)
    return [np.sum(c[:,2]), len(c)]

ppc = 16
hog_images = []
hog_features = []
for image in data_gray:
    #fd,hog_image = hog(image, orientations=8, pixels_per_cell=(ppc,ppc),cells_per_block=(4, 4),block_norm= 'L2',visualize=True)
    #hog_images.append(hog_image)
    #hog_features.append(np.concatenate((fd, [np.sum(c[:,2])])))
    hog_features.append(get_feature(image))

plt.imshow(data_gray[121])
print(hog_features[121])
#%%
plt.imshow(data_gray[11])
print(hog_features[11])
#ls = [len(h) for h in hog_features]
#hog_features = [ for x in hog_features]
#print(ls)
#%%
print(hog_features)
# %%
#clf = svm.SVM()
clf = DecisionTreeClassifier()
hog_features = np.array(hog_features)
data_frame = np.hstack((hog_features,labels.reshape(len(hog_features), 1)))
np.random.shuffle(data_frame)
#%%
percentage = 80
partition = int(len(hog_features)*percentage/100)
x_train, x_test = data_frame[:partition,:-1],  data_frame[partition:,:-1]
y_train, y_test = data_frame[:partition,-1:].ravel() , data_frame[partition:,-1:].ravel()

clf.fit(x_train,y_train)
#%%
y_pred = clf.predict(x_test)
print("Accuracy: "+str(accuracy_score(y_test, y_pred)))
print('\n')
print(classification_report(y_test, y_pred))
dump(clf, 'model.bin')

# %%
